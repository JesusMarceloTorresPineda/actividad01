/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dibujar;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * FXML Controller class
 *
 * @author Cefal
 */
public class CuadroController implements Initializable {

    
    @FXML AnchorPane root;
    ArrayList<String[]> asa = new ArrayList<>();
    int sas = 1;
    int contador = 0;
    Bresenham d = new Bresenham();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String[] nes = new String[2];
                nes[0]=String.valueOf(event.getSceneX());
                nes[1]=String.valueOf(event.getSceneY());
                dibujar(Double.parseDouble(String.valueOf(event.getSceneX())), Double.parseDouble(String.valueOf(event.getSceneY())));
                asa.add(nes);
                if(sas%2==0){
                    for(String[] ax : asa){
                        System.out.println("x="+ax[0]+" y="+ax[1]);
                    }
                    drawRectangle();
                    sas = 1;
                    asa.clear();
                }else{
                    sas++;
                }
                
            }
        });
        
    }
    
    void dibujar(double x, double y){
        Circle circulito = new Circle(x,y,2,Color.BLACK);
        root.getChildren().addAll(circulito);
    }
    
    void drawRectangle(){
        String[] pos1 = asa.get(0); // x
        String[] pos2 =asa.get(1);  // y
        double initx = Double.parseDouble(pos1[0]);
        double inity = Double.parseDouble(pos1[1]);
        double diffx = Math.abs(Double.parseDouble(pos1[0])-Double.parseDouble(pos2[0]));
        double diffy = Math.abs(Double.parseDouble(pos1[1])-Double.parseDouble(pos2[1]));
        if(initx> Double.parseDouble(pos2[0])){
            initx = Double.parseDouble(pos2[0]);
            inity = Double.parseDouble(pos2[1]);
            if(inity > Double.parseDouble(pos1[1])){
                inity = Double.parseDouble(pos1[1]);
            }else{
                inity = Double.parseDouble(pos2[1]); 
            }
        }
        if(inity > Double.parseDouble(pos2[1])){
            inity = Double.parseDouble(pos2[1]);
        }
        
        System.out.println(diffx);
        System.out.println(diffy);
        for(int x = 0;x<diffx;x++){
            for(int y = 0; y< diffy;y++ ){
                Circle circulito = new Circle(initx+x,inity+y,2,Color.BLACK);
                root.getChildren().addAll(circulito);
            }
        }
    }
    
}
