/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dibujar;

import java.awt.Point;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class FXMLDocumentController {

    @FXML
    private AnchorPane root;
    @FXML
    private Button linea;
    @FXML
    private Button cuadri;
    
    @FXML
    public void initializable(){}
    
    @FXML
    private void linea() throws IOException{
        FXMLLoader loader = new FXMLLoader(
            getClass().getResource(
                "linea.fxml"
            )
        );

        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(
            new Scene(
                (Pane) loader.load()
            )
        );
        
        LineaController controller = loader.<LineaController>getController();
        stage.show();
        
    }
    
    @FXML
    private void cuadri() throws IOException{
        FXMLLoader loader = new FXMLLoader(
            getClass().getResource(
                "cuadro.fxml"
            )
        );

        Stage stage = new Stage(StageStyle.DECORATED);
        stage.setScene(
            new Scene(
                (Pane) loader.load()
            )
        );
        
        CuadroController controller = loader.<CuadroController>getController();
        stage.show();
        
    }
}
