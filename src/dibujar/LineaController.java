/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dibujar;

import java.awt.Point;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * FXML Controller class
 *
 * @author Cefal
 */
public class LineaController implements Initializable {

    
    @FXML AnchorPane root;
    ArrayList<String[]> asa = new ArrayList<>();
    int sas = 1;
    int contador = 0;
    Bresenham d = new Bresenham();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String[] nes = new String[2];
                nes[0]=String.valueOf(event.getSceneX());
                nes[1]=String.valueOf(event.getSceneY());
                dibujar(Double.parseDouble(String.valueOf(event.getSceneX())), Double.parseDouble(String.valueOf(event.getSceneY())));
                asa.add(nes);
                if(contador>0){
                    drawLines();
                    asa.clear();
                    asa.add(nes);
                }
                contador++;
                
            }
        });
    }    
    
    void dibujar(double x, double y){
        Circle circulito = new Circle(x,y,2,Color.BLACK);
        root.getChildren().addAll(circulito);
    }
    
    void drawLines(){
        String[] pos1 = asa.get(0); // x
        String[] pos2 =asa.get(1);  // y
        System.out.println(pos1[0]);
        int posx1 = (int)Double.parseDouble(pos1[0]);
        int posx2 = (int)Double.parseDouble(pos2[0]);
        int posy1 = (int)Double.parseDouble(pos1[1]);
        int posy2 = (int)Double.parseDouble(pos2[1]);
        
        int cols = (int)root.getHeight();
        int rows = (int)root.getWidth();
        root.getWidth();
        Point[][] grid = new Point[rows][cols];
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                grid[i][j] = new Point(i, j);
        List<Point> line = d.findLine(grid, posx1, posy1, posx2, posy2);
        re(grid,line);
    }
    
    void re(Point[][] grid, List<Point> line){
        int rows = grid.length;
        int cols = grid[0].length;
 
        System.out.println("\nPlot : \n");
 
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                if (line.contains(grid[i][j])){
                    Circle circulito = new Circle(i,j,2,Color.BLACK);
                    root.getChildren().addAll(circulito);
            }else{
                    }
            }
            System.out.println();
        }
    }
    
}
